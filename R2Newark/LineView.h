//
//  LineView.h
//  R2Newark
//
//  Septa Line custom UI control
//
//  Created by Thom Loring on 3/2/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LineView : UIView

typedef NS_ENUM(NSInteger, LineViewDirection) {
    LVNorthbound,
    LVSouthbound
};

@property (nonatomic) BOOL northbound;
@property (nonatomic) double total_distance;
@property (nonatomic) NSArray *mile_markers;
@property (nonatomic) CGFloat progress;

@end
