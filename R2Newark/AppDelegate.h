//
//  AppDelegate.h
//  R2Newark
//
//  Created by Xcode on 1/30/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
