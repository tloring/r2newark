//
//  StationsViewController.m
//  R2Newark
//
//  Station List from R2 Septa Webservice (not currently used)
//
//  Created by Xcode on 2/7/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import "StationsViewController.h"
#import "StationViewCell.h"
#import "SeptaData.h"

@interface StationsViewController ()
@property (nonatomic) SeptaData *septaData;
@end

@implementation StationsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.septaData = [SeptaData sharedSeptaData];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.septaData fetchStations:^{
       NSLog(@"%s stations fetched", __PRETTY_FUNCTION__);
       NSLog(@"%s num stations: %lu", __PRETTY_FUNCTION__, (unsigned long)[self.septaData.stations count]);
       [self.tableView reloadData];
       [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
    
    //self.automaticallyAdjustsScrollViewInsets = NO;
    //self.edgesForExtendedLayout = UIRectEdgeNone;
    UIEdgeInsets inset = UIEdgeInsetsMake(20, 0, 0, 0);
    self.tableView.contentInset = inset;
    self.tableView.scrollIndicatorInsets = inset;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%s num stations: %lu", __PRETTY_FUNCTION__, (unsigned long)[self.septaData.stations count]);
    return [self.septaData.stations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StationViewCell";
    StationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = self.septaData.stations[indexPath.row];
    
    return cell;
}

@end
