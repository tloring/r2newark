//
//  SeptaData.h
//  R2Newark
//
//  Data from the R2 Web Service
//
//  Created by Xcode on 2/1/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SeptaStation;

@interface SeptaData : NSObject

@property (nonatomic) NSArray *stations;

+ (instancetype)sharedSeptaData;
- (void)fetchStations:(void (^)())doneBlock;
- (void)fetchSchedule:(NSDictionary *)parameters;
- (void)fetchScheduleFrom:(SeptaStation *)from to:(SeptaStation *)to;

@end
