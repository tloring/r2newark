//
//  SeptaLine.h
//  R2Newark
//
//  Septa Line Object, which contains Septa Stations and tracks current location on the line
//
//  Created by Thom Loring on 2/19/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

@import Foundation;

@class SeptaStation;
@class SeptaLine;
@class CLLocation;

//
// Protocol for current location updates
//

@protocol SeptaLineDelegate <NSObject>
- (void)septaLineLocationUpdate:(SeptaLine *)sender;
- (void)septaLineLocationAtStation:(SeptaStation *)station;
- (void)septaLineLocationBetweenStations:(SeptaStation *)stationA and:(SeptaStation *)stationB;
- (void)septaLineLocationClosestStation:(SeptaStation *)station;
@end

//
// Public Interface
//

@interface SeptaLine : NSObject

- (instancetype)initWithHomeStation:(NSString *)origin cityStation:(NSString *)city;

// Delegate for current location updates
@property (nonatomic, weak) id<SeptaLineDelegate> delegate;

// Station array count and access methods
@property (nonatomic, readonly) NSUInteger stationCount;
-(id)objectForKeyedSubscript:(id<NSCopying>)key;
-(id)objectAtIndexedSubscript:(NSUInteger)index;

// Inbound/Outbound Stations
@property (nonatomic) SeptaStation *homeStation; // ie, Claymont  TODO: recalc total_distance on assign
@property (nonatomic) SeptaStation *cityStation; // ie, 30th Street TODO: recalc total_distance on assign
@property (nonatomic) double total_distance;
@property (nonatomic) NSArray *sub_stations;
@property (nonatomic) NSArray *mile_markers;

// Current Location on the Line
@property (nonatomic) CLLocation *location;
@property (nonatomic) NSDate *timestamp;
@property (nonatomic, readonly) double speed_mph;
@property (nonatomic, readonly) NSString *direction_string;

//
// Methods relative to Current Location
//

/**
 Return the Station Ojbect if current location is at a station
 @return a SeptaStation, otherwise nil
 */
-(SeptaStation *)atStation;

/**
 Return the Station Ojbect closest to the current location
 @return a SeptaStation
 */
-(SeptaStation *)closestStation;

/**
 Return an array of 2 Station Ojbects of the stations closest to the current location
 @return a SeptaStation
 */
-(NSArray *)betweenStations;

/**
 Return the miles from current location to station
 @param SeptaStation
 @return miles
 */
-(double)milesFrom:(SeptaStation *)station;

-(BOOL)southOfStation:(SeptaStation *)station;
-(BOOL)northOfStation:(SeptaStation *)station;

//
// Other Station Methods
//

-(double)milesBetweenStations:(SeptaStation *)stationA and:(SeptaStation *)stationB;
-(NSArray *)stationsFrom:(SeptaStation *)a to:(SeptaStation *)b;

@end

/*
 Delegates http://stackoverflow.com/questions/626898/how-do-i-create-delegates-in-objective-c
 Documentation http://dadabeatnik.wordpress.com/2013/09/25/comment-docs-in-xcode-5/
*/