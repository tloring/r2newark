//
//  LineView.m
//  R2Newark
//
//  Septa Line custom UI control
//
//  Created by Thom Loring on 3/2/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import "LineView.h"
@import CoreText;

@implementation LineView

-(id)initWithCoder:(NSCoder*)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void) setNorthbound:(BOOL)northbound
{
    NSLog(@"%s northbound: %i", __PRETTY_FUNCTION__, self.northbound);
    _northbound = northbound;
    [self setNeedsDisplay];
}

-(void) setProgress:(CGFloat)progress
{
    NSLog(@"%s: %f", __PRETTY_FUNCTION__, progress);
    _progress = progress;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    NSLog(@"%s northbound: %i, progess %f", __PRETTY_FUNCTION__, self.northbound, self.progress);
    
    CGRect viewRect = [self bounds];
    CGFloat viewWidth = viewRect.size.width;
    CGFloat viewHeight = viewRect.size.height;
    
    CGFloat top_point_y = 5;
    CGFloat bottom_point_y = viewHeight - 15;
    CGFloat line_height = bottom_point_y - top_point_y;
    CGFloat center_x = 10; //viewWidth/2;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Line
    {
      CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
      CGContextSetLineWidth(context, 3.0);
      CGContextMoveToPoint(context, center_x, top_point_y); //start at this point
      CGContextAddLineToPoint(context, center_x, bottom_point_y); //draw to this point
      CGContextStrokePath(context);
    }
    
    // North and South Big Red Circle
    {
      CGFloat northEnd_bigRed_x = center_x - 5;
      CGFloat northEnd_bigRed_y = top_point_y;
      CGFloat southEnd_bigRed_x = center_x - 5;
      CGFloat southEnd_bigRed_y = bottom_point_y;
    
      CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
      CGContextAddEllipseInRect(context,(CGRectMake (northEnd_bigRed_x, northEnd_bigRed_y, 10.0, 10.0)));
      CGContextAddEllipseInRect(context,(CGRectMake (southEnd_bigRed_x, southEnd_bigRed_y, 10.0, 10.0)));
      CGContextDrawPath(context, kCGPathFill);
      CGContextStrokePath(context);
    
      // North And South Inner White Circle
    
      CGFloat northEnd_centerWhite_x = northEnd_bigRed_x + 2.5;
      CGFloat northEnd_centerWhite_y = top_point_y + 2.5;
      CGFloat southEnd_centerWhite_x = southEnd_bigRed_x + 2.5;
      CGFloat southEnd_centerWhite_y = bottom_point_y + 2.5;
    
      CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
      CGContextAddEllipseInRect(context,(CGRectMake (northEnd_centerWhite_x, northEnd_centerWhite_y, 5.0, 5.0)));
      CGContextAddEllipseInRect(context,(CGRectMake (southEnd_centerWhite_x, southEnd_centerWhite_y, 5.0, 5.0)));
      CGContextDrawPath(context, kCGPathFill);
      CGContextStrokePath(context);
    }
    
    // Current Location Circle
    {
      CGFloat current_circle_diameter = 10.0;
      CGFloat current_x = center_x - (current_circle_diameter/2);
      CGFloat current_y;
        
      if ( self.northbound ) {
        NSLog(@"%s NORTH", __PRETTY_FUNCTION__);
        current_y = (bottom_point_y) - (line_height * self.progress);
      } else {
        NSLog(@"%s SOUTH", __PRETTY_FUNCTION__);
        current_y = (top_point_y) + (line_height * self.progress);
      }
    
      CGContextSetFillColorWithColor(context, [UIColor blueColor].CGColor);
      CGContextSetAlpha(context, 0.5);
      CGRect curr_rect = CGRectMake(current_x, current_y, current_circle_diameter, current_circle_diameter);
      CGContextAddEllipseInRect(context, curr_rect);
      CGContextDrawPath(context, kCGPathFill);
      CGContextStrokePath(context);
    }
    
    // Mile Markers
    // TODO: Instead of markers, need the array of stations to get text.
    {
      CGFloat mile_marker_diameter = 6.0;
        
      // TODO: obvious
      NSArray *names = @[@"Claymont", @"Marcus Hook", @"Highland Avenue", @"Chester", @"Eddystone",
                         @"Crum Lynne", @"Ridley Park", @"Prospect Park", @"Norwood", @"Glenolden", @"Folcroft", @"Sharon Hill",
                         @"Curtis Park", @"Darby", @"University City", @"30th Street Station"];
        
      int index = 0;
      for (NSNumber *marker in self.mile_markers)
      {
        CGFloat rounded_marker = ([marker floatValue] * 100) / 100;
        CGFloat rounded_total = (self.total_distance * 100) / 100;
        CGFloat rounded_percent = (rounded_marker / rounded_total);
            
        CGFloat marker_x = center_x - (mile_marker_diameter/2);
        CGFloat marker_y = (bottom_point_y) - (line_height * rounded_percent) + (mile_marker_diameter/2);
            
        NSLog(@"%s mile_marker[%d] %@ x,y(%f,%f)", __PRETTY_FUNCTION__, index, marker, marker_x, marker_y);
          
        CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
        CGContextAddEllipseInRect(context,(CGRectMake (marker_x, marker_y, mile_marker_diameter, mile_marker_diameter)));
        CGContextDrawPath(context, kCGPathFill);
        CGContextStrokePath(context);
            
        // draw text
        {
          CGContextSaveGState(context);
            
          // Flip the coordinate system
          CGContextSetTextMatrix(context, CGAffineTransformIdentity);
          CGContextTranslateCTM(context, 0, self.bounds.size.height);
          CGContextScaleCTM(context, 1.0, -1.0);
            
          CGMutablePathRef path = CGPathCreateMutable(); //1
          CGPathAddRect(path, NULL, CGRectMake(20, viewHeight-marker_y-15, 200, 20));
            
          //NSString *label = [NSString stringWithFormat:@" %d (%f) %@", index, marker_y, names[index]];
          NSString *fontname = @"HelveticaNeue";
          if ( index == 0 || index == 15 )
              fontname = @"HelveticaNeue-bold";
              
          NSDictionary *attributes = @{
                                       NSForegroundColorAttributeName: [UIColor redColor],
                                       NSFontAttributeName: [UIFont fontWithName:fontname size:10.0]
                                       };
          NSAttributedString* attString = [[NSAttributedString alloc] initWithString:names[index] attributes:attributes]; //2
            
          CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)attString); //3
          CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, [attString length]), path, NULL);
          
          CTFrameDraw(frame, context); //4
            
          if ( index == 0 || index == 15 )
             CTFrameDraw(frame, context); //4
            
          CFRelease(frame); //5
          CFRelease(path);
          CFRelease(framesetter);
            
          //CGContextSetTextMatrix(context, CGAffineTransformIdentity);
          //CGContextTranslateCTM(context, 0, self.bounds.size.height);
          //CGContextScaleCTM(context, 1.0, 1.0);
            
          CGContextRestoreGState(context);
        }
          
        index++;
      }
    }
    
}

@end
