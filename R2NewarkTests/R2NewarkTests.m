//
//  R2NewarkTests.m
//  R2NewarkTests
//
//  Created by Xcode on 1/30/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SeptaLine.h"

@interface R2NewarkTests : XCTestCase
@property (nonatomic) SeptaLine *septaLine;
@end

@implementation R2NewarkTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.septaLine = [[SeptaLine alloc] init];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTAssertNotNil(self.septaLine, @"SeptaLine not nil");
}

@end
